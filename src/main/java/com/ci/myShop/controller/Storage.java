package com.ci.myShop.controller;

import java.util.HashMap;
import java.util.Map;

import com.ci.myShop.model.Item;

public class Storage {
	private Map <String, Item> itemMap;
	
	public Storage() {
		super();
		this.itemMap=new HashMap<String, Item>();
	}

	public void addItem(Item obj) {
		itemMap.put(obj.getName(), obj);
	}
	
	public Item getItem(String name) {
		Item it = this.itemMap.get(name);
		return it;
	}
		
}
