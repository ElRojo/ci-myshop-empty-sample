package com.ci.myShop.controller;

import java.util.ArrayList;
import com.ci.myShop.model.Item;

public class Shop {
	private Storage storage;
	private float cash = 0;
	
	public float getCash() {
		return cash;
	}

	public Item sell(String name) {
        Item item = this.storage.getItem(name);
        if (item == null) {
            return null;
        }
        
        item.setNbrElt(item.getNbrElt()-1);
        cash = cash+item.getPrice(); //équivalent à cash+=item.getPrice()
        
        System.out.println(item.display());
        return item;
	}

	// constructeur du Shop car méthode sans retour et qui porte le même nom que la classe
	public Shop(ArrayList<Item> items) {
		storage = new Storage();
		
		//Ajoute item de ArrayList dans storage jusqu'à ce que fin de liste
		for(int i = 0; i < items.size(); i++) {
			storage.addItem(items.get(i));
		}
		
	}
	
	public boolean buy(Item item) {
		if (cash >= item.getPrice()) {
			cash = cash - item.getPrice();
			item.setNbrElt(item.getNbrElt()+1);
			if (storage.getItem(item.getName()) == null) {
				storage.addItem(item);
			}
			return true;
		}
		else {
			return false;
		}
				 
	}

}
